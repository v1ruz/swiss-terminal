<?php

$scheme = $_SERVER['REQUEST_SCHEME'] ?? 'http';

/* use wordpress functions from the /wp/ directory */
if (!defined('WP_SITEURL')) {
    define('WP_SITEURL', $scheme . '://' . $_SERVER['HTTP_HOST'] . '/wp');
}

if (!defined('WP_HOME')) {
    define('WP_HOME', $scheme . '://' . $_SERVER['HTTP_HOST']);
}

/* use use wp-content from root directory  */
if (!defined('WP_CONTENT_DIR')) {
    define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
}

if (!defined('WP_CONTENT_URL')) {
    define( 'WP_CONTENT_URL', $scheme . '://' . $_SERVER['HTTP_HOST'] . '/wp-content' );
}
