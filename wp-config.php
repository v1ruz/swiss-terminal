<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'swissterminal');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$OfWQ$s gR1SK[WQeQ,Nj}+o*cq:s )oT(itG5N1!,jr+dl}~?,@u}SNf9E!exu:');
define('SECURE_AUTH_KEY',  '#{x<A;vZ}^&nmN%a}HJDZ!`%A_LDKYJJ.[+53_Ax0>}<>gp-`[|Uwy}w24Q&@mmD');
define('LOGGED_IN_KEY',    'B:mD|LH38s7|Q9N/VvJ&j+di8A]uBj]OC2_ LC :{2J`x]A^Q^I8Fp5d7Sj:J[^/');
define('NONCE_KEY',        'yyy~V>`0/I=H/Y9+rZ>T-#<RnwOw|hn-7cy|ao?USr[`hTI/WX(p<H++4rS>L}.A');
define('AUTH_SALT',        'vD_$UY7qYv[tw*GjI<Rva8sF~T(2l[D4#qu_8v[blmAL9bn@].PApFBBM+R@*]}q');
define('SECURE_AUTH_SALT', 'W]X%f{qa=xC,X$H(Ie;p^-yo[YNIUR#6e3C;*UmR4WxMqz;s+Bg_ja|zMX@ikh^x');
define('LOGGED_IN_SALT',   'sgUX+F!.6lE_I7IS{+L#f>x.oEll%+HtUeka@;TP]^Z98J!.+`yR}+eXP+8ZX9aS');
define('NONCE_SALT',       'Ae%*%E;|e,hzOvH{35GlzL}H!gr>wz)9;#S`d4&0O(Z,? [}.ey^[>4AC_2Wc3A ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

include_once('wp-config-urls.php');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
