<?php
add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	if (function_exists('acf_register_block')) {
		acf_register_block(array(
			'name'						=> 'buttons',
			'title'						=> __('Buttons'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'				=> 'formatting',
			'icon'						=> 'admin-site-alt',
			'keywords'				=> array(),
		));

		acf_register_block(array(
			'name'						=> 'headline',
			'title'						=> __('Headline'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'				=> 'formatting',
			'icon'						=> 'admin-site-alt',
			'keywords'				=> array(),
		));

		acf_register_block(array(
			'name'						=> 'careers',
			'title'						=> __('Careers'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'				=> 'formatting',
			'icon'						=> 'admin-site-alt',
			'keywords'				=> array(),
		));

		acf_register_block(array(
			'name'						=> 'contact',
			'title'						=> __('Contact Us'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'				=> 'formatting',
			'icon'						=> 'admin-site-alt',
			'keywords'				=> array(),
		));

		acf_register_block(array(
			'name'						=> 'services',
			'title'						=> __('Services'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'				=> 'formatting',
			'icon'						=> 'admin-site-alt',
			'keywords'				=> array(),
		));

		acf_register_block(array(
			'name'						=> 'buying-renting',
			'title'						=> __('Buying & Renting'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'				=> 'formatting',
			'icon'						=> 'admin-site-alt',
			'keywords'				=> array(),
		));

		acf_register_block(array(
			'name'						=> 'history',
			'title'						=> __('History'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'				=> 'formatting',
			'icon'						=> 'admin-site-alt',
			'keywords'				=> array(),
		));

		acf_register_block(array(
			'name'						=> 'cboxes',
			'title'						=> __('Content Box'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'				=> 'formatting',
			'icon'						=> 'admin-site-alt',
			'keywords'				=> array(),
		));

		acf_register_block(array(
			'name'						=> 'about-us',
			'title'						=> __('About Us'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'				=> 'formatting',
			'icon'						=> 'admin-site-alt',
			'keywords'				=> array(),
		));
	}
}

function my_acf_block_render_callback($block) {
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);
	$template = get_theme_file_path("/template-parts/block/content-{$slug}.php");

	// include a template part from within the "template-parts/block" folder
	if (file_exists($template)) {
		include($template);
	}
}
