<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package swissterminal_2019
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site col-md-12 col-sm-12 col-xs-12">
	<div class="row">
	<header id="masthead" class="site-header">
		<div class="container">
			<div class="site-branding col-md-3 col-sm-3 col-xs-6">
				<?php the_custom_logo(); ?>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="col-md-6 col-sm-5 col-xs-6">
				<?php
				$walker = new Menu_With_Description;
				wp_nav_menu( array(
					'theme_location'	=> 'menu-1',
					'menu_id'        	=> 'primary-menu',
					'menu_class'	 		=> 'list-unstyled list-inline',
					'walker'					=> $walker,
				) );
				?>
				<div class="mobile-navigation"></div>
			</nav><!-- #site-navigation -->
			<div class="col-md-3 col-sm-3 col-xs-6">
				<div class="slot-book">
					<a href="#"><span>SLOT BOOKING</span><img src="<?php bloginfo('template_directory'); ?>/images/booking-icon.svg"/></a>
				</div>
				<div class="lang-switch">
					<a href="#" id="de-switch">DE</a><span>|</span><a href="#" class="active" id="en-switch">EN</a>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div class="site-content"> <!-- start #content -->
