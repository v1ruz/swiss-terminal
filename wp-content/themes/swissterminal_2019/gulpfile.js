var { series, src, dest, watch, task } = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var cleancss = require('gulp-clean-css');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var webpack = require('webpack-stream');
var notify = require('gulp-notify');

function css () {
  return src('assets/sass/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 20 versions', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
    }))
    .pipe(cleancss())
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write('.'))
    .pipe(dest('css/'));
}

function script () {
  return src('assets/js/main.js')
    .pipe(sourcemaps.init())
    .pipe(webpack({
      mode: 'production',
      output: {
        filename: 'main.js',
      }
    }))
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write('.'))
    .pipe(dest('js/'));
}

function fonts () {
  return src(['./node_modules/font-awesome/fonts/*'])
    .pipe(dest('./fonts/vendor/font-awesome/'));
}

exports.default = series(fonts, function () {
  watch('assets/sass/**/*.scss', {
    ignoreInitial: false
  }, css);
  watch('assets/js/**/*.js', {
    ignoreInitial: false
  }, script);
});
