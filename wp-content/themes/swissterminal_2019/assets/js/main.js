window.jQuery = window.$ = require('jquery');
window.AOS = require('../../aos/js/aos').default;
window.debounce = require('lodash.debounce');

require('bootstrap-sass');
require('jquery-steps/build/jquery.steps');
require('../../meanmenu/jquery.meanmenu.min.js');
require('./services');
require('./equal-height');


window.isVisible = function (container, elem, partial) {
  var contHeight = container.height();

  var elemTop = $(elem).offset().top - container.offset().top;
  var elemBottom = elemTop + $(elem).height();

  var isTotal = (elemTop >= 0 && elemBottom <= contHeight);
  var isPart = ((elemTop < 0 && elemBottom > 0 ) || (elemTop > 0 && elemTop <= container.height())) && partial ;

  return  isTotal  || isPart ;
}

window.lastScrollTop = 0;
window.scrolling = false;
window.changeActiveHistory = function(e){
  if (scrolling) return;

  const history = $(this);
  const st = history.scrollTop();
  let years = history.find('.year > section').get();

  if (st <= window.lastScrollTop) { // upscroll
    years = years.reverse();
  }

  $.each(years, (k, v) => {
    if ($(v).hasClass('active')) {
      if (!isVisible(history, v, false) || st <= window.lastScrollTop) {
        if (typeof years[k + 1] != 'undefined') {
          if (isVisible(history, years[k + 1], false)) {
            $(years).removeClass('active');
            $(years[k + 1]).addClass('active');
          }
        }
      }
    }
  });

  if (st > window.lastScrollTop && (st + history.height()) >= history[0].scrollHeight) {
    $(years).removeClass('active');
    $(years).last().addClass('active');
    scrolling = true;
    setTimeout(() => {
      scrolling = false;
    }, 200);
  }

  window.lastScrollTop = st;
}
window.$('.history').on('scroll', window.changeActiveHistory);
