window.$.fn.equalHeights = function(){
  var max_height = 0;
  $(this).css('height', 'initial');
	$(this).each(function(){
		max_height = Math.max($(this).height(), max_height);
	});
	$(this).each(function(){
		$(this).height(max_height);
	});
};
