const servicesNavigation = () => {
  $('.services-nav > ul > li > a').on('click', function(e) {
    let top = 60, hash = $(this).attr('href');

    if (hash[0] == '#') {
      e.preventDefault();
      if ($(window).width() > 1024) {
        top = 90;
      }
      $('html, body').animate({ scrollTop: $(hash).offset().top - top });
    }
  });

  $('.services-list .service-content > .readmore').on('click', function() {
    $(this).css('display', 'none');
    $(this).parent().find('.hidemore').css('display', 'block');
    $(this).parent().find('> p').css('display', 'block');
  });

  $('.services-list .service-content > .hidemore').on('click', function() {
    $(this).css('display', 'none');
    $(this).parent().find('.readmore').css('display', 'block');
    $(this).parent().find('> p').removeAttr('style');
  });
}

const buyRentForm = () => {
  $('#buy-rent-form').on('click', () => {
    $('html, body').animate({ scrollTop: $('.buying-renting-steps').offset().top - 50 });
  });

  // Buy and renting forms
  const wrapper = $('.buying-renting-steps');
  if (wrapper.length) {
    window.stepsdata = [{},{},{},{}];
    window.steps = [ false, false, false, false ];
    const stepper = wrapper.find('.stepper');
    const form = stepper.closest('form');
    stepper.steps({
      headerTag: "h4",
      bodyTag: "fieldset",
      transitionEffect: "fade",
      // transitionEffectSpeed: 500,
      onStepChanging: (event, currentIndex, newIndex) => {
        if (currentIndex > newIndex) {
          return steps[newIndex];
        } else {
          return steps[currentIndex];
        }
      },
      onFinishing: (event, currentIndex) => {
        console.log(steps, stepsdata);
        return steps.every((el) => {
          return el === true;
        });
      },
      onFinished: (event, currentIndex) => {
        console.log(stepsdata);
        alert("JQuery ajax submitted here!");
      }
    });

    // Step #1
    $('input[name="type"]').on('change', function() {
      const groups = $(this).closest('.row').find('.form-group');
      stepsdata[0] = { type: $(this).val() };
      if ($(this).val() == 'Rent') {
        steps[0] = true;
        groups.slice(1).each((k, v) => {
          $(v).addClass('blur-group');
          $(v).find('input:checked').prop('checked', false);
        });
      } else {
        steps[0] = false;
        groups.slice(1, 2).removeClass('blur-group');
      }
    });
    $('input[name="storage"]').on('change', function() {
      const groups = $(this).closest('.row').find('.form-group');
      stepsdata[0]['storage'] = $(this).val();
      if ($(this).val() == 'Switzerland') {
        groups.slice(2, 3).addClass('active').removeClass('blur-group');
        groups.slice(3, 4).removeClass('active');
      } else {
        groups.slice(2, 3).removeClass('active');
        groups.slice(3, 4).addClass('active').removeClass('blur-group');
      }
      groups.slice(2, 3).find('input:checked').prop('checked', false);
      groups.slice(3, 4).find('input:checked').prop('checked', false);
      delete stepsdata[0]['used-new'];
      delete stepsdata[0]['use-abroad'];
    });
    $('input[name="used-new"], input[name="use-abroad"]').on('change', function() {
      if ($(this).val()) {
        stepsdata[0][$(this).attr('name')] = $(this).val();
        stepsdata[1]['qty'] = stepsdata[1]['qty'] || 1;
        steps[0] = true;
      }
    });

    // Step #2
    $('.qty-group > .qty-sub').on('click', function() {
      const input = $(this).next();
      const val = parseInt(input.val()) - 1;
      const min = parseInt(input.attr('min'));
      input.val(val < min ? min : val);
    });
    $('.qty-group > .qty-add').on('click', function() {
      const input = $(this).prev();
      const val = parseInt(input.val()) + 1;
      const max = parseInt(input.attr('max'));
      input.val(val > max ? max : val);
    });

    function hideSizes() {
      const use = $('input[name="use"]:checked').val();
      const art = $('input[name="art"]:checked').val();
      if (use && art) {
        if (use == 'Dry') {
          if (art == 'High Cube') {
            $('input[name="size"]').each((k, v) => {
              if ($(v).val() == '10‘') {
                $(v).parent().addClass('hidden');
              } else {
                $(v).parent().removeClass('hidden');
              }
            });
          } else {
            $('input[name="size"]').each((k, v) => {
              $(v).parent().removeClass('hidden');
            });
          }
        } else {
          $('input[name="size"]').each((k, v) => {
            if (art == 'Box') {
              if ($(v).val() == '20‘') {
                $(v).parent().removeClass('hidden');
              } else {
                $(v).parent().addClass('hidden');
              }
            } else {
              if ($(v).val() == '40‘') {
                $(v).parent().removeClass('hidden');
              } else {
                $(v).parent().addClass('hidden');
              }
            }
          });
        }
      }
      $('input[name="size"]:checked').prop('checked', false);
    }

    $('input[name="use"]').on('change', function() {
      const groups = $(this).closest('.row').find('.form-group');
      groups.slice(2, 3).removeClass('blur-group');
      stepsdata[1]['use'] = $(this).val();
      hideSizes();
    });

    $('input[name="art"]').on('change', function() {
      const groups = $(this).closest('.row').find('.form-group');
      groups.slice(3, 4).removeClass('blur-group');
      stepsdata[1]['art'] = $(this).val();
      hideSizes();
    });

    $('input[name="size"]').on('change', function() {
      stepsdata[1]['size'] = $(this).val();
      steps[1] = true;
    });

    // Step #3
    function checkPickupLocation() {
      const zip = $('input[name="zip"]').val();
      const city = $('input[name="city"]').val();
      const street = $('input[name="street"]').val();

      stepsdata[2]['zip'] = zip;
      stepsdata[2]['city'] = city;
      stepsdata[2]['street'] = street;

      return !!(zip && city && street);
    }

    $('input[name="delivery"]').on('change', function() {
      const groups = $(this).closest('.row').find('.form-group');
      if ($(this).val() != 'Collection in Frenkendorf') {
        groups.slice(1, 2).removeClass('blur-group');
        stepsdata[2]['delivery'] = $(this).val();
        steps[2] = checkPickupLocation();
      } else {
        groups.slice(1, 2).addClass('blur-group');
        steps[2] = true;
        stepsdata[2] = { delivery: $(this).val() };
      }
    });

    $('input[name="zip"], input[name="city"], input[name="street"]').on('keyup', function () {
      steps[2] = checkPickupLocation();
    });

    // Step #4
    function checkCustomerData() {
      const salutation = $('input[name="salutation"]:checked').val();
      const firstname = $('input[name="firstname"]').val();
      const lastname = $('input[name="lastname"]').val();
      const company = $('input[name="company"]').val();
      const street = $('input[name="street-nr"]').val();
      const zip = $('input[name="zip-code"]').val();
      const city = $('input[name="city-data"]').val();
      const email = $('input[name="email"]').val();
      const phone = $('input[name="phone"]').val();
      const preferred = $('select[name="preferred-contact"]').val();
      const note = $('textarea[name="note"]').val();

      stepsdata[3] = {
        salutation,
        firstname,
        lastname,
        company,
        street,
        zip,
        city,
        email,
        phone,
        preferred,
        note
      };
      if (
        salutation &&
        firstname &&
        lastname &&
        company &&
        street &&
        zip &&
        city &&
        email &&
        phone &&
        preferred
      ) {
        return true;
      }
      return false;
    }

    $('.form-control').on('keyup change', function () {
      steps[3] = checkCustomerData();
    });
  }
}

window.$(document)
  .ready(buyRentForm)
  .ready(servicesNavigation);
