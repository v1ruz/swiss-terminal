<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package swissterminal_2019
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			<ul class="list-unstyled list-inline">
				<li><a href="#">Copyright &copy; Swissterminal <?php echo date('Y'); ?></a></li>
				<li><a href="#">Privacy & Cookie</a></li>
				<li><a href="#">Policy Terms & Conditions</a></li>
				<li><a href="#">Site Map</a></li>
			</ul>
			<div class="socmed">
				<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/fb-icon.png"/></a>
				<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/linkedin-icon.png"/></a>
				<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/twitter-icon.png"/></a>
				<a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/yt-icon.png"/></a>
			</div>
			<div class="mobile-copy">
				<ul class="list-unstyled list-inline">
					<li class="copy"><a href="#">Copyright &copy; Swissterminal <?php echo date('Y'); ?></a></li>
					<li><a href="#">Privacy & Cookie</a></li>
					<li><a href="#">Policy Terms & Conditions</a></li>
					<li><a href="#">Site Map</a></li>
				</ul>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
  </div>
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript">
	AOS.init();

	function heroBanner() {
		var headerheight = $('header#masthead').outerHeight(true);
		var screenheight = $(window).height();

		if($(window).width() < 1100){
			headerheight = 63;
		}

		$('#hero').css('height', `${screenheight - headerheight}px`);
	}

	function siteNavigation() {
		var width = window.innerWidth ? window.innerWidth : $(window).width();
		var height = window.innerHeight ? window.innerHeight : $(window).height();
		var nav = $('#site-navigation');
		var menu = nav.find('#mobile-primary-menu');

		nav.find('> .hamburger').remove();

		if (width <= 1024) {
			nav.prepend('<a role="button" class="hamburger"><span></span></a>');
			menu.hide();
			menu.css('width', width);
			menu.css('height', height);

			nav.find('.menu-item-has-children').off('click');
			nav.find('.menu-item-has-children').on('click', function(e) {
				if (!$(e.target).hasClass('sub-heading') && !$(e.target).parent().hasClass('sub-heading')) {
					e.stopPropagation();

					var sub = $(this).find('> ul.sub-menu');
					var title = $(this).find('> a').html();

					sub.show().addClass('fixed animated faster slideInRight');
					sub.prepend(`<li class="sub-heading"><a role="button">${title}</a></li>`);

					setTimeout(function() {
						sub.removeClass('animated faster slideInRight');
					}, 200);
				}
			});

			$(document).off('click', 'ul.sub-menu .sub-heading');
			$(document).on('click', 'ul.sub-menu .sub-heading', function(e) {
				var mmenu = $(this).parent();
				mmenu.addClass('animated slideOutRight');

				setTimeout(function() {
					mmenu.removeClass('fixed animated slideOutRight').hide();
					mmenu.find('.sub-heading').remove();
				}, 300);
			});

			if (!$('.lang-slot-book').length) {
				$('.lang-switch').parent().append('<div class="lang-slot-book"></div>');
			}
			$('#site-navigation .mobile-navigation').append($('.lang-switch'));
			$('#site-navigation .mobile-navigation').append($('.slot-book'));
		} else {
			menu.removeAttr('style');

			if ($('.lang-slot-book').length) {
				$('.lang-slot-book').parent().append($('.slot-book'));
				$('.lang-slot-book').parent().append($('.lang-switch'));
				$('.lang-slot-book').remove();
			}
		}

		$(document).off('click', '#site-navigation > .hamburger');
		$(document).on('click', '#site-navigation > .hamburger', function() {
			var btn = $(this);

			if (btn.hasClass('active')) {
				btn.removeClass('active');
				menu.slideUp();
				menu.find('.sub-menu').removeAttr('style');
				menu.find('.sub-heading').remove();
			} else {
				btn.addClass('active');
				btn.slideDown('fast');
				menu.slideDown();
			}
		});
	}

	function cloneSiteNavigation() {
		var cloned = $('#site-navigation #primary-menu').clone();
		cloned[0].id = 'mobile-primary-menu';

		cloned.find('.mobile-not-parent').each(function(k, v) {
			$(v).parent().append($(v).find('> ul > li'));
			$(v).remove();
		});

		$('#site-navigation .mobile-navigation').append(cloned);
	}

	function requestForm() {
		if ($('.overview-form').length) {
			// var ul = $('.buying-renting-steps .steps > ul');
			// ul.find('> li > a').css('height', 'initial');
			// ul.find('> li > a').css('height', `${ul.height()}px`);
		}
	}

	$(document)
		.ready(heroBanner)
		.ready(cloneSiteNavigation)
		.ready(siteNavigation)
		.ready(requestForm);

	$(window)
		.resize(heroBanner)
		.resize(siteNavigation)
		.resize(requestForm);

	$(document).ready(function() {
		if($(window).width() < 1500){
			$(".col1 p").text(function(index, currentText) {
				return currentText.substr(0, 140);
			});
		}

		$('#scroll-down').click(function(){
			//console.log("screenh " + screenh);
			var subtr = $('#scroll-up').offset().top - 170;
			if($(window).width() < 1100){
				subtr = $('#scroll-up').offset().top + 15;
			}
			$('html, body').animate({
				scrollTop: subtr
			}, 1000, function(){
			});
		});

		$('#scroll-up').click(function(){
			$('html, body').animate({
				scrollTop: 0
			}, 1500, function(){
			});
		});

		$(window).resize(function(){
			if($(window).width() < 1367){
				$(".col1 p").text(function(index, currentText) {
					return currentText.substr(0, 140);
				});
			}
		});

		$('.mean-expand').click(function(){
			var mean = $(this);

			if (mean.hasClass('mean-clicked')) {
				if (mean.closest('ul.mean-active').length) {
					mean.closest('ul.mean-active').find('> li.mean-active > a').hide();
				}

				mean.closest('li').addClass('mean-active animated faster slideInRight');
				mean.prev().prev().addClass('open');

				setTimeout(function() {
					mean.closest('li').removeClass('slideInRight');
				}, 300);
			} else {
				mean.closest('li').removeClass('mean-active slideInRight');
				mean.closest('li').addClass('slideOutLeft');
				mean.prev().prev().removeClass('open');

				setTimeout(function() {
					mean.closest('li').removeClass('slideOutLeft');
				}, 300);

				if (mean.closest('ul.mean-active').length) {
					mean.closest('ul.mean-active').find('> li.mean-active > a').show();
				}
			}
		})
	});

var scrollAmount = 0;
var pixelsToNotify = 10;

$(document).scroll(function() {
  var distance = $(window).scrollTop();
    if(distance - scrollAmount > pixelsToNotify){
			$('#masthead').addClass('sticky');
			scrollAmount = distance;
    }
    if(distance == 0){
			$('#masthead').removeClass('sticky');
			scrollAmount = distance;
    }
});
</script>
</body>
</html>
