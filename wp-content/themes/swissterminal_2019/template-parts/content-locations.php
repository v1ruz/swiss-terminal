<div class="page-content">
	<div class="container">
		<div class="headline">
			Connecting Switzerland To The World
		</div>
		<div class="description">
			As a landlocked country, access to the world’s seas is critical for Switzerland. That’s where we come in.
		</div>
	</div>
	<?php
		$locations = [
			[
				'address' => 'Basel-Birsfelden',
				'headline' => 'Trimodal Container Depot on the River Rhine',
				'description' => 'Our trimodal terminal Birsfelden is located just outside the city limits of Basel. Along with its optimal position on the river Rhine, this facility features excellent connections to rail and road networks.',
				'address-list' => [
					'Swissterminal AG',
					'Hafenstrasse 14',
					'4127 Birsfelden',
					'Switzerland'
				],
				'keydata' => [
					[
						[
							'key' => 'Surface area',
							'data' => '15,600 m2'
						],
						[
							'key' => 'Storage Capacity',
							'data' => '2,000 TEU'
						]
					],
					[
						[
							'key' => 'Reefer Connections',
							'data' => '36'
						],
						[
							'key' => '4 tracks, total length AGL',
							'data' => '560 m'
						]
					],
					[
						[
							'key' => 'Road',
							'data' => 'Autobahn A2'
						],
						[
							'key' => 'vehicles',
							'data' => ['truck', 'train', 'ferry']
						]
					],
					[
						[
							'key' => 'Mail address',
							'data' => 'Swissterminal AG Flachsackerstrasse 7 4402 Frenkendorf Switzerland'
						]
					],
				],
			],
			[
				'address' => 'Basel-Frenkendorf',
				'headline' => 'The Rail Gateway to Switzerland',
				'description' => 'Our facility in Frenkendorf is not only our headquarters, but also the largest privately operated container terminal in Switzerland. This bimodal terminal (rail/road) is directly located on the European north-south corridor, with excellent links to both directions.',
				'address-list' => [
					'Swissterminal AG',
					'Flachsackerstrasse 7',
					'4402 Frenkendorf',
					'Switzerland'
				],
				'keydata' => [
					[
						[
							'key' => 'Surface area',
							'data' => '45,000 m2'
						],
						[
							'key' => 'Storage Capacity',
							'data' => '5,000 TEU'
						],
						[
							'key' => 'Handling with Gantry Crane',
							'data' => '34t'
						],
					],
					[
						[
							'key' => 'Reefer Connections',
							'data' => '36'
						],
						[
							'key' => '4 tracks, total length AGL',
							'data' => '1600 m'
						]
					],
					[
						[
							'key' => 'Road',
							'data' => 'Autobahn A2/A3'
						],
						[
							'key' => 'vehicles',
							'data' => ['truck', 'train']
						]
					],
					[
						[
							'key' => 'Physical and postal address',
							'data' => 'Swissterminal AG Flachsackerstrasse 7 4402 Frenkendorf Switzerland'
						]
					],
				],
			],
			[
				'address' => 'Basel-Kleinhueningen',
				'headline' => 'The Barge Gateway to Switzerland',
				'description' => 'Our trimodal location in Basel connects Switzerland and neighbouring regions in Germany and France to Europe’s largest container ports by barge.',
				'address-list' => [
					'Swissterminal AG',
					'Westquaistrasse 12',
					'4019 Basel',
					'Switzerland'
				],
				'keydata' => [
					[
						[
							'key' => 'Surface area',
							'data' => '12,000 m2'
						],
						[
							'key' => 'Storage Capacity',
							'data' => '1,700 TEU'
						]
					],
					[
						[
							'key' => 'Reefer Connections',
							'data' => '36'
						],
						[
							'key' => '4 tracks, total length AGL',
							'data' => '420 m'
						]
					],
					[
						[
							'key' => 'Road',
							'data' => 'Autobahn A2<br/>Rail connection<br/>Access to river Rhine'
						],
						[
							'key' => 'vehicles',
							'data' => ['truck', 'train', 'ferry']
						]
					],
					[
						[
							'key' => 'Mail address',
							'data' => 'Swissterminal AG Flachsackerstrasse 7 4402 Frenkendorf Switzerland'
						]
					],
				],
			],
			[
				'address' => 'Basel-Liestal',
				'headline' => 'The Biggest Empty Truck Centre in Switzerland',
				'description' => 'The facility in Liestal was built in 2008 to reduce congestion at existing depots for empty container depots in Switzerland. Today, this is the biggest empty truck centre in the country.',
				'address-list' => [
					'Swissterminal AG',
					'Unterfeldstrasse 13',
					'4410 Liestal',
					'Switzerland'
				],
				'keydata' => [
					[
						[
							'key' => 'Surface area',
							'data' => '20,000 m2'
						],
						[
							'key' => 'Storage Capacity',
							'data' => '5,000 TEU'
						]
					],
					[
						[
							'key' => 'Road',
							'data' => 'Autobahn A2/A3<br/>Truck connections – access to main highways in the area or something along those lines.'
						],
						[
							'key' => 'vehicles',
							'data' => ['truck']
						]
					],
					[
						[
							'key' => 'Postal address',
							'data' => 'Swissterminal AG Flachsackerstrasse 7 4402 Frenkendorf Switzerland'
						]
					],
				],
			],
			[
				'address' => 'Zurich-Niederglatt',
				'headline' => 'Efficient Operations in the Heart of Europe',
				'description' => 'Located just outside Zurich’s city limits, this bimodal terminal offers efficient container handling for both rail and truck and features efficient connections to locations in Switzerland, Germany and Austria. Zurich-Niederglatt is linked to all European main sea ports via the gateway in Basel-Frenkendorf and offers direct rail connections to the port of Rotterdam and Antwerp.',
				'address-list' => [
					'Swissterminal AG',
					'Industriestrasse 139',
					'8155 Niederhasli',
					'Switzerland'
				],
				'keydata' => [
					[
						[
							'key' => 'Surface area',
							'data' => '34,000 m2'
						],
						[
							'key' => 'Storage Capacity',
							'data' => '4,000 TEU'
						]
					],
					[
						[
							'key' => 'Reefer Connections',
							'data' => '12'
						],
						[
							'key' => '4 tracks, total length AGL',
							'data' => '1100 m'
						]
					],
					[
						[
							'key' => 'Road',
							'data' => 'Autobahn A1/A51'
						],
						[
							'key' => 'vehicles',
							'data' => ['truck', 'train']
						]
					],
					[
						[
							'key' => 'Mail address',
							'data' => 'Swissterminal AG Flachsackerstrasse 7 4402 Frenkendorf Switzerland'
						]
					],
				],
			],
		]
	?>
	<div class="location-tabs">
		<div class="location-nav">
			<ul class="nav nav-pills nav-justified">
				<?php foreach ($locations as $key => $location) { ?>
					<li class="<?= $key ? '' : 'active' ?>"><a href="#location-<?= $key ?>" data-toggle="collapse" data-parent="#accordion" aria-expanded="<?= $key ? 'false' : 'true' ?>"><?= $location['address'] ?></a></li>
				<?php } ?>
			</ul>
		</div>
		<div class="panel-group" id="accordion">
			<?php foreach ($locations as $key => $location) { ?>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" data-toggle="collapse" data-parent="#accordion" href="#location-<?= $key ?>" aria-expanded="<?= $key ? 'false' : 'true' ?>" aria-controls="location-<?= $key ?>">
					<h4 class="panel-title">
						<a role="button">
							<?= $location['address'] ?>
						</a>
					</h4>
				</div>
				<div id="location-<?= $key ?>" class="panel-collapse collapse <?= $key ? '' : 'in' ?>" role="tabpanel" aria-expanded="<?= $key ? 'false' : 'true' ?>">
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-7 col-lg-6">
								<div class="headline"><?= $location['headline'] ?></div>
								<div class="address-list">
									<ul>
									<?php
									foreach ($location['address-list'] as $address) {
										echo '<li>' . $address . '</li>';
									}
									?>
									</ul>
								</div>
								<div class="description"><?= $location['description'] ?></div>
							</div>
							<div class="col-sm-5 col-lg-6">
								<img src="<?= get_template_directory_uri() ?>/images/terminal-basel.png" alt="" class="img-responsive">
							</div>
						</div>
						<div class="row">
							<div class="col-xs-7 col-lg-12">
								<div class="address">
									<?php
									foreach ($location['address-list'] as $address) {
										echo $address . '<br/>';
									}
									?>
								</div>
							</div>
							<div class="col-xs-5 col-lg-12">
								<div class="btn-keydata">
									<a role="button" class="collapsed" data-toggle="collapse" data-target="#keydata-<?= $key ?>-0" aria-expanded="false"></a>
									<span>Key Data</span>
								</div>
							</div>
						</div>
						<div class="keydata collapse" id="keydata-<?= $key ?>-0" aria-expanded="false">
							<div class="row">
								<div class="col-xs-6">
								<?php foreach ($location['keydata'] as $key => $keydata) { ?>
									<div class="keydata-list">
										<?php foreach ($keydata as $d) { ?>
										<div>
											<?php if ($d['key'] == 'vehicles') { ?>
												<svg class="truck <?= in_array('truck', $d['data']) ? '' : 'hidden' ?>">
													<use xlink:href="<?= get_template_directory_uri() ?>/images/st-icons.svg#truck"></use>
												</svg>
												<svg class="train <?= in_array('train', $d['data']) ? '' : 'hidden' ?>">
													<use xlink:href="<?= get_template_directory_uri() ?>/images/st-icons.svg#train"></use>
												</svg>
												<svg class="ferry <?= in_array('ferry', $d['data']) ? '' : 'hidden' ?>">
													<use xlink:href="<?= get_template_directory_uri() ?>/images/st-icons.svg#ferry"></use>
												</svg>
											<?php } else { ?>
												<strong><?= $d['key'] ?></strong>
												<?= $d['data'] ?>
											<?php } ?>
										</div>
										<?php } ?>
									</div>
								<?php if ($key % 2 == 1) { echo '</div><div class="col-xs-6">'; } ?>
								<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>

	<div class="container network-links">
		<div class="row">
			<div class="col-md-6 col-lg-5">
				<div class="headline">Global Reach,  Local Presence.</div>
				<div class="title">Swissterminal Network Links</div>
				<div class="description">
					Our network links provide fast, reliable and regular connections to all corners of Europe, helping bring your business ambitions to life. Welcome aboard!
				</div>
			</div>
			<div class="col-md-6 col-lg-7">
				<div class="img-network">
					<img src="<?= get_template_directory_uri() ?>/images/network-links.svg" alt="" class="img-responsive">
				</div>
			</div>
		</div>
		<div class="legend-link">
			<table>
				<tr>
					<td>Mode:</td>
					<td>Operator:</td>
				</tr>
				<tr>
					<td class="train">
						<svg>
							<use xlink:href="<?= get_template_directory_uri() ?>/images/st-icons.svg#train"></use>
						</svg>
					</td>
					<td>
						<ul>
							<li class="red">Schweizerzug</li>
							<li class="blue">PSA</li>
							<li class="pink">Hannibal</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td class="ferry">
						<svg>
							<use xlink:href="<?= get_template_directory_uri() ?>/images/st-icons.svg#ferry"></use>
						</svg>
					</td>
					<td>
						<ul>
							<li class="c3c">C3C</li>
							<li class="light-blue">Dubbelman</li>
							<li class="sch">Schweizerzug</li>
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
