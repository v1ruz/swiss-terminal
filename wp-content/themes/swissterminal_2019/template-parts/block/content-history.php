<div class="history-wrapper">
	<div class="headline">Our History</div>
	<div class="history">
		<div id="timeline">
			<div>
				<?php $key = 0; ?>
				<?php while (have_rows('history')) { the_row(); ?>
				<section class="year">
					<h3><?= get_sub_field('year') ?></h3>
					<?php while (have_rows('timeline')) { the_row(); ?>
					<section class="<?= $key ? '' : 'active' ?>">
						<h4><?= get_sub_field('date') ?></h4>
						<ul>
						<?php while (have_rows('events')) { the_row(); ?>
							<li><?= get_sub_field('event') ?></li>
						<?php } ?>
						</ul>
						<div class="proof">
							<img src="<?= get_sub_field('image') ?>" alt="<?= get_sub_field('date') ?>" class="img-responsive">
						</div>
					</section>
					<?php $key++; } ?>
				</section>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
