<div class="board text-center">
	<div class="container">
		<div class="headline"><?= get_field('headline') ?></div>
		<div class="heading"><?= get_field('heading') ?></div>
		<p class="description"><?= get_field('description') ?></p>
	</div>
</div>
<div class="container executives">
	<div class="row">
		<?php while (have_rows('executives')) { the_row(); ?>
		<div class="col-xs-6 col-md-4">
			<img src="<?= get_sub_field('profile') ?>" alt="<?= get_sub_field('name') ?>" class="img-responsive">
			<span class="name"><?= get_sub_field('name') ?></span>
			<span class="title"><?= get_sub_field('position') ?></span>
		</div>
		<?php } ?>
	</div>
</div>
<div class="btn-contactus">
	<a href="<?= get_field('button') ?>" class="btn btn-block btn-contact"><?= get_field('button_label') ?></a>
</div>
