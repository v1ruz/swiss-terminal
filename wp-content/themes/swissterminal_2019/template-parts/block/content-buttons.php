<div class="container">
	<div class="btn-wrapper">
		<div class="row">
			<?php if (!get_field('switch')) { ?>
				<div class="col-sm-6">
					<a href="<?php the_field('left_button') ?>" class="btn btn-block btn-explore"><?php the_field('left_button_label') ?></a>
				</div>
			<?php } ?>
			<div class="col-sm-6">
				<a href="<?php the_field('right_button') ?>" class="btn btn-block btn-contact"><?php the_field('right_button_label') ?></a>
			</div>
			<?php if (get_field('switch')) { ?>
				<div class="col-sm-6">
					<a href="<?php the_field('left_button') ?>" class="btn btn-block btn-explore"><?php the_field('left_button_label') ?></a>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
