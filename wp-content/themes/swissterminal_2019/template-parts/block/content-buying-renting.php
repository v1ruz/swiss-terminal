<div class="overview-form">
	<?php
	$containers = [
		[
			'hash' => 'nine-m-container',
			'size' => '9m³',
			'type' => 'Container'
		],
		[
			'hash' => 'ten-steel-container',
			'size' => "10'",
			'type' => 'Steel Container'
		],
		[
			'hash' => 'twenty-box-container',
			'size' => "20'",
			'type' => 'Box Container'
		],
		[
			'hash' => 'twenty-highcube-container',
			'size' => "20'",
			'type' => 'Highcube Container'
		],
		[
			'hash' => 'forty-box-container',
			'size' => "40'",
			'type' => 'Box Container'
		],
		[
			'hash' => 'forty-highcube-container',
			'size' => "40'",
			'type' => 'Highcube Container'
		],
		[
			'hash' => 'twenty-opentop-container',
			'size' => "20'",
			'type' => 'Opentop Container'
		],
		[
			'hash' => 'forty-opentop-container',
			'size' => "40'",
			'type' => 'Opentop Container'
		],
		[
			'hash' => 'twenty-container',
			'size' => "20'",
			'type' => 'Container',
			'feature' => true
		],
		[
			'hash' => 'forty-container',
			'size' => "40'",
			'type' => 'Container',
			'feature' => true
		],
	];
	?>
	<div class="container-tabs">
		<?php if (have_rows('containers')) { $key = 0; ?>
		<ul class="nav nav-pills nav-justified">
			<?php while (have_rows('containers')) { the_row(); ?>
				<li class="<?= $key ? '' : 'active' ?>">
					<a href="#<?= sanitize_title_with_dashes(get_sub_field('type') . (int)get_sub_field('size')) ?>" data-toggle="tab" aria-expanded="false" class="<?= get_sub_field('highlight') ? 'feature' : '' ?>">
						<span><strong><?= get_sub_field('size') ?></strong> <?= get_sub_field('type') ?></span>
					</a>
				</li>
			<?php $key ++; } ?>
		</ul>
		<?php } ?>
	</div>
	<div class="buying-renting-description">
		<?php if (have_rows('containers')) { $key = 0; ?>
		<div class="tab-content">
			<?php while (have_rows('containers')) { the_row(); ?>
				<div id="<?= sanitize_title_with_dashes(get_sub_field('type') . (int)get_sub_field('size')) ?>" class="tab-pane fade<?= $key ? '' : ' in active' ?>">
					<div class="container">
						<div class="box-container">
							<div class="box-title">
								<?= get_sub_field('size') ?> <?= get_sub_field('type') ?>
							</div>
							<div class="box-description"><?= get_sub_field('description') ?></div>
						</div>
						<div class="box-technical">
							<div class="row">
								<div class="col-md-6">
									<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">
										<!-- Wrapper for slides -->
										<div class="carousel-inner" role="listbox">
											<?php if (have_rows('images')) { $images = 0;  ?>
												<?php while (have_rows('images')) { the_row(); ?>
												<div class="item <?= $images ? '' : 'active' ?>">
													<img src="<?= get_sub_field('image') ?>">
												</div>
												<?php $images++; } ?>
											<?php } ?>
										</div>

										<!-- Controls -->
										<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
									</div>
								</div>
								<div class="col-md-6">
									<div class="technical-data">
										<div class="data-title">
										Technical Data
										</div>
										<div class="data-description">
										<?php if (have_rows('technical_data')) { ?>
											<?php while (have_rows('technical_data')) { the_row(); ?>
												<div class="container-info">
													<strong><?= get_sub_field('heading') ?></strong>
													<?php if (have_rows('informations')) { ?>
														<?php while (have_rows('informations')) { the_row(); ?>
															<span><?= get_sub_field('info') ?></span>
														<?php } ?>
													<?php } ?>
												</div>
											<?php } ?>
										<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php $key ++; } ?>
		</div>
		<?php } ?>
	</div>
	<div class="buying-renting-goto">
		<a role="button" id="buy-rent-form">
			Buy or Rent
			<span class="fa fa-angle-down"></span>
		</a>
	</div>
	<div class="buying-renting-steps">
		<form action="" method="post">
			<div class="overview">
				<svg>
					<use xlink:href="<?= get_template_directory_uri() ?>/images/st-icons.svg#download"></use>
				</svg>
				Container Overview Download
			</div>
			<div class="stepper">
				<h4>Buying &amp; Renting</h4>
				<fieldset>
					<legend>Request Form</legend>
					<div class="row">
						<div class="col-md-5">
							<div class="form-group">
								<label class="title">Do you want to rent or buy a container?</label>
								<label class="radio">
									<input type="radio" name="type" value="Buy" required>
									<span>Buy</span>
								</label>
								<label class="radio">
									<input type="radio" name="type" value="Rent" required>
									<span>Rent</span>
								</label>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group blur-group">
								<label class="title">Storage</label>
								<p class="short-desc">Is the container stored or exported in Switzerland?</p>
								<label class="radio">
									<input type="radio" name="storage" value="Switzerland" required>
									<span>Switzerland</span>
								</label>
								<label class="radio">
									<input type="radio" name="storage" value="Export" required>
									<span>Export</span>
								</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group blur-group conditional active">
								<label class="title">Used or New?</label>
								<label class="radio">
									<input type="radio" name="used-new" value="Used" required>
									<span>Used</span>
								</label>
								<label class="radio">
									<input type="radio" name="used-new" value="New" required>
									<span>New</span>
								</label>
							</div>
							<div class="form-group blur-group conditional">
								<label class="title">Use abroad</label>
								<label class="radio">
									<input type="radio" name="use-abroad" value="Only bearings">
									<span>Only bearings (without CSC approval)</span>
								</label>
								<label class="radio">
									<input type="radio" name="use-abroad" value="For storage and transport">
									<span>For storage and transport (required CSC approval)</span>
								</label>
							</div>
						</div>
					</div>
				</fieldset>
				<h4>Container</h4>
				<fieldset>
					<legend>Request Form</legend>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label class="title">Quantity</label>
								<div class="input-group qty-group">
									<span class="input-group-addon qty-sub">-</span>
									<input type="text" class="form-control" name="qty" value="1" min="1" max="100">
									<span class="input-group-addon qty-add">+</span>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="title">Use</label>
								<label class="radio">
									<input type="radio" name="use" value="Dry">
									<span>Dry (Normal)</span>
								</label>
								<label class="radio">
									<input type="radio" name="use" value="Refrigerator">
									<span>Refrigerator (Reefer)</span>
								</label>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group blur-group">
								<label class="title">Art</label>
								<label class="radio">
									<input type="radio" name="art" value="Box">
									<span>Box</span>
								</label>
								<label class="radio">
									<input type="radio" name="art" value="High Cube">
									<span>High Cube</span>
								</label>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group blur-group">
								<label class="title">Size</label>
								<label class="radio">
									<input type="radio" name="size" value="10‘">
									<span>10‘</span>
								</label>
								<label class="radio">
									<input type="radio" name="size" value="20‘">
									<span>20‘</span>
								</label>
								<label class="radio">
									<input type="radio" name="size" value="40‘">
									<span>40‘</span>
								</label>
							</div>
						</div>
					</div>
				</fieldset>
				<h4>Delivery</h4>
				<fieldset>
					<legend>Request Form</legend>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="title">Delivery / pickup?</label>
								<label class="radio">
									<input type="radio" name="delivery" value="Collection in Frenkendorf">
									<span>Collection in <strong>Frenkendorf</strong></span>
								</label>
								<label class="radio">
									<input type="radio" name="delivery" value="Delivery with unloading">
									<span>Delivery <strong>with</strong> unloading</span>
								</label>
								<label class="radio">
									<input type="radio" name="delivery" value="Delivery without unloading">
									<span>Delivery <strong>without</strong> unloading</span>
								</label>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group blur-group">
								<label class="title">Place of delivery</label>
								<div class="row field-information">
									<label class="input col-xs-6">
										<input type="text" name="zip" class="form-control" placeholder="ZIP Code">
									</label>
									<label class="input col-xs-6">
										<input type="text" name="city" class="form-control" placeholder="City">
									</label>
									<label class="input col-xs-12">
										<input type="text" name="street" class="form-control" placeholder="Street / Nr">
									</label>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
				<h4>Customer Data</h4>
				<fieldset>
					<legend>Request Form</legend>
					<div class="row field-information">
						<div class="col-xs-12 col-md-12">
							<div class="form-group">
								<label class="radio radio-inline">
									<input type="radio" name="salutation" value="Mr.">
									<span>Mr.</span>
								</label>
								<label class="radio radio-inline">
									<input type="radio" name="salutation" value="Ms.">
									<span>Ms.</span>
								</label>
								<label class="radio radio-inline">
									<input type="radio" name="salutation" value="Mrs.">
									<span>Mrs.</span>
								</label>
								<label class="radio radio-inline">
									<input type="radio" name="salutation" value="Miss">
									<span>Miss</span>
								</label>
							</div>
						</div>
						<div class="col-xs-6 col-md-6">
							<input type="text" class="form-control" name="firstname" placeholder="First Name">
						</div>
						<div class="col-xs-6 col-md-6">
							<input type="text" class="form-control" name="lastname" placeholder="Last Name">
						</div>
						<div class="col-xs-6 col-md-6">
							<input type="text" class="form-control" name="company" placeholder="Company">
						</div>
						<div class="col-xs-6 col-md-6">
							<input type="text" class="form-control" name="street-nr" placeholder="Street / Nr">
						</div>
						<div class="col-xs-6 col-md-6">
							<input type="text" class="form-control" name="zip-code" placeholder="ZIP Code">
						</div>
						<div class="col-xs-6 col-md-6">
							<input type="text" class="form-control" name="city-data" placeholder="City">
						</div>
						<div class="col-xs-12 col-md-6">
							<input type="text" class="form-control" name="email" placeholder="Email">
						</div>
						<div class="col-xs-6 col-md-6">
							<input type="text" class="form-control" name="phone" placeholder="Phone">
						</div>
						<div class="col-xs-6 col-md-6">
							<select name="preferred-contact" class="form-control">
								<option value="">- Preferred Contact -</option>
								<option value="By Email">By Email</option>
								<option value="By Phone">By Phone</option>
							</select>
						</div>
						<div class="col-xs-12 col-md-6">
							<textarea class="form-control" name="note" placeholder="Note"></textarea>
						</div>
					</div>
				</fieldset>
			</div>
		</form>
	</div>
</div>
