<?php
/*
$services = [
	[
		'hash' => 'container-handling',
		'title' => 'Container Handling',
		'sub' => 'Optimising Your Supply Chain',
		'content' => "As the Swiss market leader for container terminal services, we are at the forefront of developing innovative solutions for container handling, fusing our well-positioned terminal network and our expertise with pioneering digital solutions.
		\nWith locations in Basel, Birsfelden, Frenkendorf, Niederglatt and Liestal, our terminals provide fast, reliable and regular connections to north and south Europe. In addition, our terminals are also equipped with Europe’s first inland terminal online slot booking system, helping slash waiting times for your container bookings and increasing uptime for your business.
		\nWhile helping you beat or meet your deadlines is important, we realise it’s still only half of our job. The other half is being proactive about ensuring your cargo is safe and secure. That’s why we’re experts in everything from documenting the cold chain of reefers to monitoring and regulations for dangerous goods.
		\nAt the end of the day, we’re here to be your trusted interface between shippers, shipping lines and carriers. Anything you need, we’re here to help.
		",
		'pointers' => [
			'Five container terminals: Basel, Birsfelden, Frenkendorf, Niederglatt and Liestal',
			'Efficient container depot services, supported by online slot booking',
			'Value added services, such as reefer services, storage, maintenance and repair',
		],
	],
	[
		'hash' => 'container-depo',
		'title' => 'Container Depot / Storage',
		'sub' => 'Giving Your Business Space to Grow',
		'content' => "Our storage services can save you time and money as well as provide additional flexibility, for example, by allowing you the capacity to take advantage of bulk buying/transport economies.
		\nAt all of our terminals, space for both empty and full containers is available at long periods and low costs. We offer storage for standard and reefer containers, and we can also temporarily store dangerous goods containers.
		\nOnce we’ve figured out the right storage option for you, our dedicated team of experts will properly inspect and thoroughly maintain all your containers. That way, your job stays streamlined and hassle free.
		",
		'pointers' => [
			'Five container terminals: Basel, Birsfelden, Frenkendorf, Niederglatt and Liestal',
			'Storage for standard and reefer containers',
			'Temporary storage for hazardous goods containers',
			'Inspection, cleaning and repair of dry and reefer containers',
		],
	],
	[
		'hash' => 'container-maintenance',
		'title' => 'Container Maintenance / Repair',
		'sub' => 'Maintaining your Capital ',
		'content' => "Regular maintenance and repair can add years to your container’s life and save you significant expense on new containers. We pride ourselves on providing top quality repair services at the best rates, using only the most highly trained technicians.
		\nAt our locations in Basel, Birsfelden, Frenkendorf and Niederglatt, we clean, maintain and repair all types of containers – including reefers and tank containers – in accordance with the international rules of the Container Safety Convention (CSC). As an industry leader, we are certified by SGS ISO 9001:2015 and work according to the Swissterminal Quality Management (SQM).
		\nWith us, no repair is too small or too complex, and we’re always on hand for an emergency.
		",
		'pointers' => [
			'Five locations: Basel, Birsfelden, Frenkendorf, Liestal, Niederglatt',
			'Maintenance and repair of dry, reefer and tank containers',
			'Fully trained technicians',
			'SGS ISO 9001:2015 qualified',
		],
	],
	[
		'hash' => 'container-reefer',
		'title' => 'Container Reefer Services',
		'sub' => 'Preserving your Commodities',
		'content' => "Reliable, safe passage of perishable commodities is vital for the success of your business, and maintaining your cooling unit is key to ensuring your goods arrive in the exact condition intended. We help you to keep cool.
		\nAt each of our terminals, we offer services for temperature-controlled containers, and with our mobile Swissterminal Reefer Service, we can service your refrigerated containers on-site, even at locations across the Swiss border.
		\nLet us make sure your cold chain is connected from end to end.
		",
		'pointers' => [
			'Reefer storage',
			'Pre-trip inspection',
			'Monitoring and setting adjustment',
			'Mobile and on-site repair',
		],
	]
];
*/
?>
<div class="services-nav">
	<ul>
		<?php if (have_rows('containers')) { ?>
			<?php while (have_rows('containers')) { the_row(); ?>
				<li>
					<a href="#<?= sanitize_title_with_dashes(get_sub_field('title')) ?>"><?php the_sub_field('title') ?></a>
				</li>
			<?php } ?>
		<?php } ?>
		<?php if (have_rows('other_services')) { ?>
			<?php while (have_rows('other_services')) { the_row(); ?>
				<li>
					<a href="#<?= sanitize_title_with_dashes(get_sub_field('title')) ?>"><?php the_sub_field('title') ?></a>
				</li>
			<?php } ?>
		<?php } ?>
		<?php if (have_rows('buy_or_rent')) { ?>
			<?php while (have_rows('buy_or_rent')) { the_row(); ?>
				<li><a href="<?= get_sub_field('link') ?>"><?= get_sub_field('label') ?></a></li>
			<?php } ?>
		<?php } ?>
	</ul>
</div>
<div class="container">
	<?php if (have_rows('containers')) { $counter = 0; ?>
	<div class="services-list">
		<?php while (have_rows('containers')) { the_row(); ?>
			<div class="row <?= $counter % 2 ? 'odd' : 'even' ?>">
				<div class="col-lg-6">
					<img src="<?= get_sub_field('image') ?>" class="img-responsive" alt="<?= get_sub_field('title') ?>">
				</div>
				<div class="col-lg-6">
					<strong class="service-title" id="<?= sanitize_title_with_dashes(get_sub_field('title')) ?>"><?= get_sub_field('title') ?></strong>
					<p class="service-sub"><?= get_sub_field('sub_title') ?></p>
					<div class="service-content">
						<?= get_sub_field('content') ?>
						<span class="readmore">Read More</span>
						<span class="hidemore">Less</span>
					</div>
					<ul class="pointers">
						<?php while (have_rows('pointers')) { the_row(); ?>
							<li>
								<svg class="corner">
									<use xlink:href="<?= get_template_directory_uri() ?>/images/st-icons.svg#corner"></use>
								</svg>
								<?= get_sub_field('content') ?>
							</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		<?php $counter ++; } ?>
	</div><!-- .services-list -->
	<?php } ?>
</div>
<div class="container other-services">
	<?php if (have_rows('other_services')) { ?>
		<?php while (have_rows('other_services')) { the_row(); ?>
			<div class="row">
				<div class="col-lg-4">
					<strong class="service-sub" id="<?= sanitize_title(get_sub_field('title')) ?>"><?php the_sub_field('title') ?></strong>
				</div>
				<?php while (have_rows('services')) { the_row(); ?>
					<div class="col-lg-4">
						<div class="headline"><?= get_sub_field('title') ?></div>
						<div class="description"><?= get_sub_field('content') ?></div>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
	<?php } ?>
</div>
