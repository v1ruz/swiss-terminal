<div class="container">
	<div class="content-boxes">
		<?php while (have_rows('boxes')) { the_row(); ?>
			<div class="row">
				<div class="col-lg-6">
					<img src="<?= get_sub_field('image') ?>" class="img-responsive" alt="<?= get_sub_field('title') ?>">
				</div>
				<div class="col-lg-6">
					<div class="headline"><?= get_sub_field('heading') ?></div>
					<div class="heading"><?= get_sub_field('title') ?></div>
					<div class="description"><?= get_sub_field('content') ?></div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
