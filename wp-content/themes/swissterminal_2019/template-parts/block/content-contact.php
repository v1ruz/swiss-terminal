<div class="container">
	<div class="row">
		<div class="col-lg-6 contact-info">
			<?php if (have_rows('content')) { ?>
				<?php while (have_rows('content')) { the_row(); ?>
				<div class="telephone"><?php the_sub_field('left') ?></div>
				<div class="address"><?php the_sub_field('right') ?></div>
				<?php } ?>
			<?php } ?>
		</div>
		<div class="col-lg-6">
			<form action="" class="contact-form" method="post">
				<div class="row">
					<div class="col-xs-6">
						<input type="text" name="firstname" class="form-control" placeholder="First name">
					</div>
					<div class="col-xs-6">
						<input type="text" name="familyname" class="form-control" placeholder="Family name">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<input type="email" name="email" class="form-control" placeholder="Email">
					</div>
					<div class="col-xs-6">
						<input type="text" name="phone" class="form-control" placeholder="Phone">
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<textarea name="message" class="form-control" placeholder="Write your message..."></textarea>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-7">
						<?php echo do_shortcode('[social_media]') ?>
					</div>
					<div class="col-xs-5">
						<button type="submit">
							Send
							<svg class="corner">
								<use xlink:href="<?= get_template_directory_uri() ?>/images/st-icons.svg#corner"></use>
							</svg>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
