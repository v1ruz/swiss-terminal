<?php $socials = get_field('social_media', 'option'); ?>
<?php foreach ($socials as $social) { ?>
	<a href="<?php echo $social['url'] ?>" class="icons">
		<svg class="<?php echo $social['type'] ?>">
			<use xlink:href="<?= get_template_directory_uri() ?>/images/st-icons.svg#<?php echo $social['type'] ?>"></use>
		</svg>
	</a>
<?php } ?>
