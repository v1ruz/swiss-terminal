<div class="page-content">
	<div class="container">
		<div class="description">
			Here you can find important documents for download:
		</div>
		<div class="download-list">
			<ul>
				<li>Terms and conditions</li>
				<li>Tariff for terminal services</li>
				<li>Dangerous goods at Swissterminal sites</li>
				<li>Holiday schedule 2019</li>
				<li>Online registration – information for trucking companies</li>
			</ul>
			<ul>
				<li>Slot booking – information for trucking companies</li>
				<li>Safety regulations for truck drivers in the terminal</li>
				<li>Reefer alarm checklist </li>
				<li>Certificates</li>
				<li>Mission statement</li>
			</ul>
		</div>
	</div>
</div>
