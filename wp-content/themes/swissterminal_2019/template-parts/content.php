<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package swissterminal_2019
 */

?>

<div class="page-content">
	<?php the_content() ?>
</div>
