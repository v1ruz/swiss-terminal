<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package swissterminal_2019
 */

?>

<article id="post-<?php the_ID(); ?>" class="col-md-6 col-sm-6 col-xs-12">
	
	<?php if(has_post_thumbnail()){?>
		<a href="<?php the_permalink(); ?>">
			<?php swissterminal_2019_post_thumbnail(); ?>
		</a>
	<?php } else {?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<img src="<?php bloginfo('template_directory'); ?>/images/image-placeholder.jpg">
			</a>
		</div>
	<?php } ?>
	<div class="title-wrap">
		<h4>
			<span class="pull-left">
				<?php echo get_the_date(); ?>
			</span>
			<span class="pull-right">
				social-media
			</span>
		</h4>
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	</div>
	<a href="<?php the_permalink(); ?>">
	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->
	</a>
</article><!-- #post-<?php the_ID(); ?> -->
