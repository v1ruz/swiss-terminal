<?php
/**
 * The template for displaying homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package swissterminal_2019
 */

get_header();
?>

	<div id="content" class="content-area col-md-12 col-sm-12 col-xs-12">
		<main id="main" class="site-main row">

			<div id="hero" style="background: url('http://charlesallandev.com/swissterminal/wp-content/uploads/2019/08/hero-web-lower-res.png')no-repeat top right; background-size: cover;">
				<div class="container">
					<div class="caption-top">
						<p><strong>Keeping your cargo connected and your business moving</strong></p>
						<p>As the Swiss market leader for container terminal services, we’re uniquely positioned to help you achieve better supply chain efficiency, reliably and flexibility. That way, your cargo stays on track, and your business stays ahead.</p>
					</div>
				</div>
				<div class="caption-bottom">
					<img src="<?php bloginfo('template_directory'); ?>/images/connect.svg"/>
				</div>
				<div id="scroll-down"><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon-down.svg"></div>
			</div>
			<div id="content-proper" class="container">
				<div class="row">
					<div id="scroll-up"><img src="<?php bloginfo('template_directory'); ?>/images/arrow-icon-up.svg"></div>
					<section class="home-block-wrapper">
					<div class="home-block-inner">
					<div class="col1 home-block col-md-6 col-sm-6 col-xs-12 aos-item"  data-aos="fade-up">
						<div class="inner" style="background-color: red; height: 100%;">
							<h3>Container Handling</h3>
							<p>With four locations near the commercial centres of Zurich and Basel, we are the leader in maritime container handling in Switzerland, providing fast, reliable and regular connections to north and south Europe.</p>
							<div class="button-area">
								<ul class="slider-nav list-inline list-unstyled">
									<li><a class="active" href="#"></a></li>
									<li><a href="#"></a></li>
									<li><a href="#"></a></li>
									<li><a href="#"></a></li>
								</ul>
								<a class="btn" href="#"><span>Learn more</span><img src="<?php bloginfo('template_directory'); ?>/images/arrow-white.png"/></a>
							</div>
						</div>
					</div>
					<div class="col2 home-block col-md-6 col-sm-6 col-xs-12 aos-item" data-aos="fade-up" data-aos-delay="100">
						<div  class="inner" style="background-color: #b1b1b1; background-image: url('<?php bloginfo("template_directory"); ?>/images/hero-services.png'); background-size: cover; background-repeat:  no-repeat; background-position: center center; height: 100%; width: 100%; padding: 0;">
							<!-- <a href="#"><img src="<?php //bloginfo('template_directory'); ?>/images/watch-video.png"></a> -->
							<!-- <a href="#"><img src="<?php //bloginfo('template_directory'); ?>/images/our-services-btn-01.svg"></a> -->
						</div>
					</div>
					<div class="col1 home-block col-md-6 col-sm-6 col-xs-12 aos-item" data-aos="fade-up" data-aos-delay="200">
						<div class="inner" style="background-color: #ECECEC; height: 100%; width: 100%;">
							<div class="absolute">
								<h2>Swiss Links...</h2>
								<img src="<?php bloginfo('template_directory'); ?>/images/swiss-links.svg" class="link-route pull-right">
								<img src="<?php bloginfo('template_directory'); ?>/images/mode-operator-05.png" class="link-legend pull-left">
							</div>
						</div>
					</div>
					<div class="col2 home-block col-md-6 col-sm-6 col-xs-12 aos-item" data-aos="fade-up" data-aos-delay="300">
						<div class="inner" style="background-image: url('<?php bloginfo("template_directory"); ?>/images/goodnews-magazin.png'); background-position: left center; background-repeat: no-repeat; background-size: cover; background-color: #434254; height: 100%; width: 100%;">
							<h3 class="pull-right" style="text-align: left; width: auto;">Goodnews<br>Issue 15</h3>
							<a class="btn" href="#"><span>View Full Issue</span><img src="<?php bloginfo('template_directory'); ?>/images/arrow-white.png"/></a>
					</div>
					</div>
					</section>
					<div class="st-links">
						<ul id="st-link" class="nav nav-pills nav-justified">
							<li class="active"><a href="#basel" data-toggle="tab">Basel - Kleinhueningen</a></li>
							<li><a href="#frenkendorf" data-toggle="tab">Basel - Frenkendorf</a></li>
							<li><a href="#birs" data-toggle="tab">Basel - Birsfelden</a></li>
							<li><a href="#liestal" data-toggle="tab">Basel - Liestal</a></li>
							<li><a href="#niederglatt" data-toggle="tab">Zurich - Niederglatt</a></li>
						</ul>
						<div class="tab-content">
							<div id="basel" class="tab-pane fade in active">
								<div class="panel panel-default">
						        <div class="panel-heading">
						          <h4 class="panel-title">
						            <a data-toggle="collapse" data-parent=".tab-pane" href="#baselbirsfelden">Basel - Kleinhueningen
						            </a>
						          </h4>
						        </div>
						        <div id="baselbirsfelden" class="panel-collapse collapse in">
						          <div class="panel-body">
						            <div class="leftcol col-md-6 col-sm-6 col-xs-5">
									<div style="background: url('<?php bloginfo("template_directory"); ?>/images/Terminal-Basel-Swissterminal-Vogelsperspektive_2_ca.png')no-repeat center center; background-size: cover; height: 380px; width: 100%;"></div>
									</div>
									<div class="rightcol col-md-6 col-sm-6 col-xs-7">
										<p><span class="red">The barge gateway to Switzerland</span><br>Our location in Basel connects Switzerland and neighbouring regions in Germany and France to Europe’s largest container ports by barge, rail and road.</p>
										<a class="btn-wrap" href="#"><span>Read more</span><img src="<?php bloginfo('template_directory'); ?>/images/arrow-red.png"/></a>
									</div>
						          </div>
						        </div>
						      </div>
							</div>
							<div id="frenkendorf" class="tab-pane fade">
								<div class="panel panel-default">
						        <div class="panel-heading">
						          <h4 class="panel-title">
						            <a data-toggle="collapse" data-parent=".tab-pane" href="#baselfrenkendor">Basel - Frenkendorf
						            </a>
						          </h4>
						        </div>
						        <div id="baselfrenkendor" class="panel-collapse collapse">
						          <div class="panel-body">
						            <div class="leftcol col-md-6 col-sm-6 col-xs-5">
									<div style="background: url('<?php bloginfo("template_directory"); ?>/images/Terminal-Basel-Swissterminal-Vogelsperspektive_2_ca.png')no-repeat center center; background-size: cover; height: 380px; width: 100%;"></div>
									</div>
									<div class="rightcol col-md-6 col-sm-6 col-xs-7">
										<p><span class="red">The rail gateway to Switzerland</span><br>Our facility in Frenkendorf is not only our headquarters, but also the largest privately operated container terminal in Switzerland. This bimodal terminal […]</p>
										<a class="btn-wrap" href="#"><span>Read more</span><img src="<?php bloginfo('template_directory'); ?>/images/arrow-red.png"/></a>
									</div>
						          </div>
						        </div>
						      </div>
							</div>
							<div id="liestal" class="tab-pane fade">
								<div class="panel panel-default">
						        <div class="panel-heading">
						          <h4 class="panel-title">
						            <a data-toggle="collapse" data-parent=".tab-pane" href="#baselliestal">Basel - Liestal
						            </a>
						          </h4>
						        </div>
						        <div id="baselliestal" class="panel-collapse collapse">
						          <div class="panel-body">
						            <div class="leftcol col-md-6 col-sm-6 col-xs-5">
									<div style="background: url('<?php bloginfo("template_directory"); ?>/images/Terminal-Basel-Swissterminal-Vogelsperspektive_2_ca.png')no-repeat center center; background-size: cover; height: 380px; width: 100%;"></div>
									</div>
									<div class="rightcol col-md-6 col-sm-6 col-xs-7">
										<p><span class="red">The biggest empty truck centre in Switzerland</span> The facility in Liestal was built in 2008 to reduce congestion at empty container depots in Switzerland.</p>
										<a class="btn-wrap" href="#"><span>Read more</span><img src="<?php bloginfo('template_directory'); ?>/images/arrow-red.png"/></a>
									</div>
						          </div>
						        </div>
						      </div>
							</div>
							<div id="birs" class="tab-pane fade">
								<div class="panel panel-default">
						        <div class="panel-heading">
						          <h4 class="panel-title">
						            <a data-toggle="collapse" data-parent=".tab-pane" href="#baselbirs">Basel - Birsfelden
						            </a>
						          </h4>
						        </div>
						        <div id="baselbirs" class="panel-collapse collapse">
						          <div class="panel-body">
						            <div class="leftcol col-md-6 col-sm-6 col-xs-5">
									<div style="background: url('<?php bloginfo("template_directory"); ?>/images/Terminal-Basel-Swissterminal-Vogelsperspektive_2_ca.png')no-repeat center center; background-size: cover; height: 380px; width: 100%;"></div>
									</div>
									<div class="rightcol col-md-6 col-sm-6 col-xs-7">
										<p><span class="red">Trimodal container depot on the river Rhine.</span> Our trimodal terminal Birsfelden is located just outside the city limits of Basel. Along with its optimal position on the river Rhine […]</p>
										<a class="btn-wrap" href="#"><span>Read more</span><img src="<?php bloginfo('template_directory'); ?>/images/arrow-red.png"/></a>
									</div>
						          </div>
						        </div>
						      </div>
							</div>
							<div id="niederglatt" class="tab-pane fade">
								<div class="panel panel-default">
						        <div class="panel-heading">
						          <h4 class="panel-title">
						            <a data-toggle="collapse" data-parent=".tab-pane" href="#baselniederglatt">Zurich - Niederglatt
						            </a>
						          </h4>
						        </div>
						        <div id="baselniederglatt" class="panel-collapse collapse">
						          <div class="panel-body">
						            <div class="leftcol col-md-6 col-sm-6 col-xs-5">
									<div style="background: url('<?php bloginfo("template_directory"); ?>/images/Terminal-Basel-Swissterminal-Vogelsperspektive_2_ca.png')no-repeat center center; background-size: cover; height: 380px; width: 100%;"></div>
									</div>
									<div class="rightcol col-md-6 col-sm-6 col-xs-7">
										<p><span class="red">Efficient operations in the heart of Europe</span> Located just outside Zurich’s city limits, this bimodal terminal offers efficient container handling for both rail and truck and features efficient […]</p>
										<a class="btn-wrap" href="#"><span>Read more</span><img src="<?php bloginfo('template_directory'); ?>/images/arrow-red.png"/></a>
									</div>
						          </div>
						        </div>
						      </div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
