<?php
/**
 * Template Name: Goodnews
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package swissterminal_2019
 */

get_header();
?>

	<div id="page-inner" class="content-area">
		<main id="main" class="site-main">
			<div class="page-header">
				<div class="container">
					<?php
					if ($parent = wp_get_post_parent_id($post->ID)) {
						echo '<h1 class="entry-title">';
						echo get_the_title($parent);
						echo '</h1>';
					} else {
						the_title( '<h1 class="entry-title">', '</h1>' );
					}
					?>
				</div>
			</div>
		<div class="page-content">
		  <div class="container">
			<div class="search-area">
				<div class="pull-left">
					<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
					    <label>
					        <span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
					        <input type="search" class="search-field"
					            placeholder="<?php echo esc_attr_x( 'What would you like to read about?', 'placeholder' ) ?>"
					            value="<?php echo get_search_query() ?>" name="s"
					            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
					    </label>
					    <input type="submit" class="search-submit"
					        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" />
					</form>
				</div>
				<div class="pull-right">
					<a href="#" id="goodnews-btn">Read Our Goodnews Magazine</a>
				</div>
			</div>
			<div class="search-filters">
				<ul class="list-unstyled list-inline">
					<li><a href="#">All</a></li>
					<li><a href="#">Terminals</a></li>
					<li><a href="#">Containers</a></li>
					<li><a href="#">Links</a></li>
					<li><a href="#">Technology</a></li>
					<li><a href="#">Finance</a></li>
					<li><a href="#">Services</a></li>
					<li><a href="#">Environment</a></li>
					<li><a href="#">Safety</a></li>
					<li><a href="#">Events</a></li>
					<li><a href="#">Advisories</a></li>
					<li><a href="#">Shipping</a></li>
					<li><a href="#">Switzerland</a></li>
				</ul>
			</div>
			<?php
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

			$args = array(
			'orderby'        => 'date',
		    'post_type'      => 'post',
		    'post_status'    => 'publish',
		    'posts_per_page' => 4,
		    'paged' => $paged
			);
			$the_query = new WP_Query( $args );

			if ( $the_query->have_posts() ) :
			/* Start the Loop */
			while ( $the_query->have_posts() ) :
				$the_query->the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content-post');

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
			<div class="news-pagination">
				<div class="pagination-inner">
			<?php
			$big = 999999999; // need an unlikely integer
			$translated = __( 'Page', 'mytextdomain' ); // Supply translatable string

			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '?paged=%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' => $the_query->max_num_pages,
			    'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>',
			    'prev_text'          => __('<img class="prevbtn" src="http://charlesallandev.com/swissterminal/wp-content/uploads/2019/09/arrow-left.png">'),
				'next_text'          => __('<img class="nextbtn" src="http://charlesallandev.com/swissterminal/wp-content/uploads/2019/09/arrow-right.png">'),
			) ); ?>
				</div>
			</div>

		  </div>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
