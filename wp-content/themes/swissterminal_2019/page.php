<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package swissterminal_2019
 */
get_header();
?>

	<main id="page-inner" class="<?= $post->post_name ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="page-header" style="background-image: url(<?= get_the_post_thumbnail_url() ?>)">
				<div class="container">
					<?php
					if ($parent = wp_get_post_parent_id($post->ID)) {
						echo '<h1 class="entry-title">';
						echo get_the_title($parent);
						echo '</h1>';
					} else {
						the_title( '<h1 class="entry-title">', '</h1>' );
					}
					?>
				</div>
			</div>
			<?php
				while (have_posts()) { the_post();
					get_template_part( 'template-parts/content', $post->post_name );
				}
			?>
		</article><!-- #post-<?php the_ID(); ?> -->
	</main><!-- #main -->

<?php
get_footer();
