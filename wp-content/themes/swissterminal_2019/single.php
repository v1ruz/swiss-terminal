<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package swissterminal_2019
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<div id="page-inner" class="content-area">
		<main id="main" class="site-main">
			<div class="page-header">
				<div class="container">
					<?php
					if ($parent = wp_get_post_parent_id($post->ID)) {
						echo '<h1 class="entry-title">';
						echo get_the_title($parent);
						echo '</h1>';
					} else {
						echo( '<h1 class="entry-title">News</h1>' );
					}
					?>
				</div>
			</div>
		<div class="page-content">
		  <div class="container">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content-postsingle');

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

 		</div>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
